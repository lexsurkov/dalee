drop table if exists users;
create table users
(
    id       bigint unsigned auto_increment primary key,
    username varchar(255) not null,
    constraint users_username_unique unique (username)
);

drop table if exists products;
create table products
(
    id         bigint unsigned auto_increment primary key,
    name       varchar(255) not null,
    user_id    bigint unsigned null,
    created_at datetime not null default current_timestamp,

    index (created_at),
    constraint products_name_unique unique (name),
    constraint products_user_id_foreign
    foreign key (user_id) references users (id)
        on update cascade on delete cascade
);

drop table if exists categories;
create table categories
(
    id   bigint unsigned auto_increment primary key,
    name varchar(255) not null,
    constraint categories_name_unique unique (name)
);

create table product_category
(
    product_id  bigint unsigned not null,
    category_id bigint unsigned not null,
    constraint product_category_product_id_category_id_unique
        unique (product_id, category_id),
    constraint product_category_category_id_foreign
        foreign key (category_id) references categories (id),
    constraint product_category_product_id_foreign
        foreign key (product_id) references products (id)
);

lock tables users write;
insert into users(username) values ('login1');
insert into users(username) values ('login2');
insert into users(username) values ('login3');
insert into users(username) values ('login4');
insert into users(username) values ('login5');
unlock tables;

lock tables categories write;
insert into categories(name) values ('category1');
insert into categories(name) values ('category2');
insert into categories(name) values ('category3');
insert into categories(name) values ('category4');
insert into categories(name) values ('category5');
unlock tables;

lock tables products write;
insert into products(name, user_id) values ('product1', 1);
insert into products(name,user_id) values ('product2', 2);
insert into products(name) values ('product3');
insert into products(name) values ('product4');
insert into products(name) values ('product5');
unlock tables;

lock tables product_category write;
insert into product_category(product_id, category_id) values (1, 1);
insert into product_category(product_id, category_id) values (1, 2);
insert into product_category(product_id, category_id) values (1, 3);
insert into product_category(product_id, category_id) values (2, 1);
insert into product_category(product_id, category_id) values (3, 3);
unlock tables;

-- Список товаров с датами публикации и именами публикаторов
create view view_product_list as
    select p.id, p.name, p.created_at, u.username from products p
    left join users u on p.user_id=u.id;

-- Список всех категорий товаров и количества товаров в каждой из них
create view view_product_count as
    select c.id, c.name, count(pc.product_id) as count from categories c
    left join product_category pc on pc.category_id=c.id
    group by c.id, c.name;

-- Список пользователей, которые опубликовали хотя бы один товар за последние N дней, и количество товаров, которые опубликовал этот пользователь за это время
CREATE FUNCTION getNday() RETURNS int(11) RETURN @nDay;
create view view_product_user as
    select u.id, u.username, count(p.id) as count from users u
    inner join products p on u.id = p.user_id
        and date(p.created_at)>=date(date_sub(current_timestamp, interval getNday() day))
    group by u.id, u.username;

-- Список всех пользователей с указанием товара, который они опубликовали последним, если таковой есть, и дата этой публикации
create view view_product_last as
select u.*, product.name, product.created_at from users u
left join (
    select p.* from products p
    inner join (
        select user_id, max(created_at) as created_at
        from products
        group by user_id
    ) as latest on p.user_id = latest.user_id and p.created_at = latest.created_at
) as product on u.id = product.user_id;