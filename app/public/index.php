<?php

defined('DB_DSN') or define('DB_DSN', 'mysql:dbname=' . getenv('DB_DATABASE') . ';host=' . getenv('DB_HOST'));
defined('DB_USERNAME') or define('DB_USERNAME', getenv('DB_USERNAME'));
defined('DB_PASSWORD') or define('DB_PASSWORD', getenv('DB_PASSWORD'));

defined('APP_DIR') or define('APP_DIR', __DIR__.'/../src');

require(APP_DIR . '/BaseApplication.php');

new src\Application();

