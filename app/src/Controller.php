<?php
namespace src;

class Controller
{
    public function __construct() {}

    public function render($name, array $data = [])
    {
        $content = $this->renderPhp($name, $data);
        require('views/main.php');
    }

    public function renderAjax($name, array $data = [])
    {
        $controller = $this;

        foreach($data as $key=>$value)
            $$key = $value;

        require('views/'.$name.'.php');
    }

    public function renderPhp($name, array $data = [])
    {
        ob_start();
        ob_implicit_flush(false);
        $controller = $this;

        foreach($data as $key=>$value)
            $$key = $value;

        require('views/'.$name.'.php');
        return ob_get_clean();
    }

    public function getQueryParam($name, $defaultValue = null)
    {
        $params = $_GET;
        return isset($params[$name]) && !empty($params[$name])? $params[$name] : $defaultValue;
    }
}