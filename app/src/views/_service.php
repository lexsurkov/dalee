<?php
/**
 * @var string $orig
 * @var string $text
 */
?>
<div id="content">
    <h2>Оригинальный текст</h2>
    <p><?= $orig ?></p>
    <h2>Измененный текст</h2>
    <p><?= $text ?></p>
</div>
