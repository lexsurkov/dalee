<?php
/** @var array $items */
?>
<script>
    function onClick(e)
    {
        $.get( "/site/refresh-service?url="+$(".input").val(), function( data ) {
            document.querySelector('#content').innerHTML = data;
        });
    }
</script>
<div class="search-div">
    <input type="search" name="" placeholder="Введите url" class="input" />
    <input onclick="onClick(this)" type="button" name="" value="Обновить" class="search-button"/>
</div>
<div id="content">
</div>
