<?php
/** @var array $items */
?>
<script>
    window.onload = function () {
        onClick();
    }
    function onClick(e)
    {
        $.get( "/site/refresh-product-user?day="+$(".input").val(), function( data ) {
            document.querySelector('#content').innerHTML = data;
        });
    }
</script>
<div class="search-div">
    <input type="search" name="" placeholder="Введите кол-во дней" class="input" />
    <input onclick="onClick(this)" type="button" name="" value="Обновить" class="search-button"/>
</div>
<div id="content">
</div>
