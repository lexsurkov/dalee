<?php

/**
 * @var string $title
 * @var array $items
 */


$header = count($items) ? array_keys($items[0]) : [];
?>
<h2><?= $title ?></h2>
<div>
    <table class="table">
        <thead>
        <tr>
            <?php foreach ($header as $value):?>
                <th><?= $value ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($items as $item):?>
            <tr>
            <?php foreach ($item as $value):?>
            <td><?= $value ?></td>
            <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>


