<?php
/** @var string $content */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8" />
    <title>Простой сайт</title>
    <link rel="stylesheet" type="text/css" href="/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <?= $content ?>
</div>
</body>
</html>


