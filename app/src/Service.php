<?php
namespace src;

class Service
{
    private $url;
    private $text;

    public function __construct(string $url = '')
    {
        $this->url = $url;
    }

    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    public function getData(): string
    {
        $client = curl_init();
        curl_setopt($client, CURLOPT_HEADER, true);
        curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($client, CURLOPT_CONNECTTIMEOUT, 1000);
        curl_setopt($client, CURLOPT_TIMEOUT, 10000);
        curl_setopt($client, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208');
        curl_setopt($client, CURLOPT_URL, $this->url);
        if (!$data = curl_exec($client)) {
            throw new \Exception(curl_error($client));
        }
        $body = substr($data, curl_getinfo($client, CURLINFO_HEADER_SIZE));
        $this->text = $body;
        return $body;
    }

    private function replace(array $sym)
    {
        $this->text = str_replace(array_keys($sym), $sym, $this->text);
    }

    public function replaceTextMatch(array $sym)
    {
        $this->replace($sym);;
    }

    public function replaceText(string $key, string $value)
    {
        $this->replace([$key => $value]);
    }

    public function getText()
    {
        return $this->text;
    }
}