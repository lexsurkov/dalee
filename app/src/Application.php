<?php
namespace src;

class Application
{
    public $defaultController = 'site';
    public $defaultAction = 'index';

    public function __construct()
    {
        $url = explode('/',$_SERVER['REQUEST_URI']);
        $controllerClass = $this->getControllerClass($url[1]);
        $action = $this->getAction($controllerClass, $url[2] ?? null);
        $controller = new $controllerClass();
        $controller->$action();
    }

    public function getControllerClass($route)
    {
        $route = substr($route,0, ($len = strpos($route,"?")) ? $len : strlen($route));
        if (!empty($route)){
            $file = APP_DIR.'/controllers/'.ucfirst($route).'Controller.php';
            if (!is_file($file)){
                die('Not found page');
            }
        }
        return 'src\controllers\\'.ucfirst(!empty($route) ? $route : $this->defaultController).'Controller';
    }

    public function getAction($controllerClass, $route)
    {
        $actions = get_class_methods($controllerClass);
        $route = substr($route,0, ($len = strpos($route,"?")) ? $len : strlen($route));
        if ($names = explode('-', $route)) {
            $route = array_shift($names);
            foreach ($names as $name) {
                $route .= ucfirst($name);
            }
        }

        return 'action'.ucfirst(
                ($route && array_search('action'.ucfirst($route), $actions))
                    ? $route : $this->defaultAction);
    }

}