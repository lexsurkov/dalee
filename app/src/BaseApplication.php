<?php

class BaseApplication
{
    public static function autoload($className) {
        $className = '../'.str_replace("\\", '/', $className).'.php';
        if (!is_file($className))
            return false;

        include($className);
        return true;
    }
}
// Загружаем необходимые файлы
spl_autoload_register(['BaseApplication', 'autoload'], true, true);
