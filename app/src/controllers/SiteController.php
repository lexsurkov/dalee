<?php

namespace src\controllers;

use src\Controller;
use src\lib\DB;
use src\Service;

class SiteController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionUsers()
    {
        $items = DB::getAll('select * from users');
        $this->render('table', [
            'title' => 'Таблица пользователи',
            'items' => $items
        ]);
    }

    public function actionProducts()
    {
        $items = DB::getAll('select * from products');
        $this->render('table', [
            'title' => 'Таблица товары',
            'items' => $items
        ]);
    }

    public function actionCategories()
    {
        $items = DB::getAll('select * from categories');
        $this->render('table', [
            'title' => 'Таблица категории',
            'items' => $items
        ]);
    }

    public function actionProductList()
    {
        $items = DB::getAll('select * from view_product_list');
        $this->render('table', [
            'title' => 'Список товаров с датами публикации и именами публикаторов',
            'items' => $items
        ]);
    }

    public function actionProductCount()
    {
        $items = DB::getAll('select * from view_product_count');
        $this->render('table', [
            'title' => 'Список всех категорий товаров и количества товаров в каждой из них',
            'items' => $items
        ]);
    }

    public function actionProductUser()
    {
        $this->render('product-user');
    }

    public function actionRefreshProductUser()
    {
        $day = $this->getQueryParam('day', 0);

        DB::set("set @nDay=$day");
        $items = DB::getAll("select * from view_product_user");
        $this->renderAjax('table', [
            'title' => "Список пользователей, которые опубликовали хотя бы один товар за последние $day дней, и количество товаров, которые опубликовал этот пользователь за это время",
            'items' => $items
        ]);
    }

    public function actionProductLast()
    {
        $items = DB::getAll('select * from view_product_last');
        $this->render('table', [
            'title' => 'Список всех пользователей с указанием товара, который они опубликовали последним, если таковой есть, и дата этой публикации',
            'items' => $items
        ]);
    }

    public function actionService()
    {
        $this->render('service');
    }

    public function actionRefreshService()
    {
        $url = $this->getQueryParam('url', '');
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new \Exception('Не верно введен Url');
        }

        $service = new Service();
        $service->setUrl($url);
        $service->getData();
        $text = $service->getText();
        $service->replaceText('Как получить текст', 'Не получить');
        $service->replaceTextMatch([
            'Добрый день' => 'Добрый вечер!'
        ]);

        $this->renderAjax('_service', [
            'orig' => $text,
            'text' => $service->getText(),
        ]);
    }
}